-- Create Databases
CREATE DATABASE IF NOT EXISTS `hsm`;
CREATE DATABASE IF NOT EXISTS `hsm_test`;

-- Create root user and grant rights
-- GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';
GRANT ALL ON *.* TO 'hsm'@'%' IDENTIFIED BY 'test1234' WITH GRANT OPTION;