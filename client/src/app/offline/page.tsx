import Image from "next/image";

export default function Offline() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <h3>You are currently offline</h3>
    </main>
  );
}
